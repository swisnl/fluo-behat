<?php

namespace Fluo\Behat\EnvVar\ServiceContainer;

use Behat\Testwork\ServiceContainer\Extension;
use Behat\Testwork\ServiceContainer\ExtensionManager;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class EnvVarExtension implements Extension {

  public function getConfigKey(): string {
    return 'fluo_env_var';
  }

  public function initialize(ExtensionManager $extensionManager): void {

  }

  public function configure(ArrayNodeDefinition $builder) {

  }

  public function load(ContainerBuilder $container, array $config): void {
    foreach (getenv() as $name => $value) {
      $container->setParameter('env.' . strtolower($name), $value);
    }
  }

  public function process(ContainerBuilder $container): void {

  }

}
