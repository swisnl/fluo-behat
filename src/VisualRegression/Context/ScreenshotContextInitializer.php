<?php

namespace Fluo\Behat\VisualRegression\Context;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\Initializer\ContextInitializer;

class ScreenshotContextInitializer implements ContextInitializer {

  private array $parameters;

  public function __construct(array $parameters) {
    $this->parameters = $parameters;
  }

  public function initializeContext(Context $context): void {
    if (!$context instanceof ScreenshotContext) {
      return;
    }

    $context->setScreenshotDir($this->parameters['screenshot_dir']);
    $context->setReferenceDir($this->parameters['reference_dir']);
    $context->setDevices($this->parameters['devices']);
  }

}
