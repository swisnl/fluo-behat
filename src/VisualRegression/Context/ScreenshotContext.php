<?php

namespace Fluo\Behat\VisualRegression\Context;

use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\MinkExtension\Context\RawMinkContext;
use DMore\ChromeDriver\ChromeDriver;
use DMore\ChromeDriver\ChromePage;
use Imagick;
use ReflectionProperty;
use RuntimeException;

class ScreenshotContext extends RawMinkContext {

  private array $devices;

  private string $screenshotDir;
  private string $referenceDir;

  private string $currentFeature;
  private string $currentScenario;

  private string $currentDevice;

  public function setScreenshotDir(string $dir): void {
    $this->screenshotDir = $dir;
  }

  public function setReferenceDir(string $dir): void {
    $this->referenceDir = $dir;
  }

  public function setDevices(array $devices): void {
    uasort($devices, fn (array $a, array $b) => $a['width'] <=> $b['width']);

    $this->devices = array_reverse($devices, TRUE);;
  }

  /**
   * @BeforeScenario
   */
  public function prepareForScreenshots(BeforeScenarioScope $scope) {
    if (!$scope->getScenario()->hasTag('javascript')) {
      return;
    }

    $this->currentFeature = substr(strstr($scope->getFeature()->getFile(), '/features/'), 10, -8);
    $this->currentScenario = mb_strtolower(str_replace(' ', '-', $scope->getScenario()->getTitle()));
    $this->ensureDirectory(sprintf(
      '%s/%s/%s',
      $this->screenshotDir,
      $this->currentFeature,
      $this->currentScenario
    ));
    $this->ensureDirectory(sprintf(
      '%s/%s/%s',
      $this->referenceDir,
      $this->currentFeature,
      $this->currentScenario
    ));

    $this->getChromeDriver()->reset();
    $this->getChromePage()->send('Network.setCacheDisabled', ['cacheDisabled' => true]);
  }

  /**
   * @Then the block :block looks like the reference screenshot
   */
  public function compareBlockWithReferenceScreenshots($block): void {
    $threshold = 0;
    $generate_missing = true;
    $generate_all = false;

    $errors = 0;
    foreach ($this->devices as $device => $info) {
      echo sprintf("%3s (%4d x %4d)", $device, $info['width'], $info['height']);

      $this->emulateDevice($device);

      $name = $this->getScreenshotName();
      $screenshot = $this->getScreenshotPath($name);
      $reference = $this->getReferencePath($name);

      if ((!file_exists($reference) && $generate_missing) || $generate_all) {
        $tries = $this->captureAndStoreScreenshotOfBlock($block, $screenshot, $reference);
        echo sprintf(" GENERATED REFERENCE (%d tries)\n", $tries);
        continue;
      }

      $diffs = $this->captureAndCompareScreenshotOfBlock($block, $screenshot, $reference, $threshold);
      $diff_best = min($diffs);
      $diff_worst = max($diffs);
      $diff_count = count($diffs);
      if ($diff_best > $threshold) {
        $errors++;
        $result = 'ERROR';
      }
      else {
        $result = 'MATCH';
      }
      echo " $result (tries: $diff_count; best: $diff_best; worst: $diff_worst)\n";
    }

    if ($errors) {
      throw new RuntimeException(sprintf('Block does not look like reference screenshot for %d devices', $errors));
    }
  }

  public function captureAndstoreScreenshotOfBlock(string $block, string $screenshot, string $reference): int {
    $diff_threshold = 0;
    $consecutive_match_threshold = 2;
    $max_tries = 9;

    $n = 0;
    $consecutive_match_count = 0;

    while ($n < $max_tries) {
      $n++;
      $screenshot_n = $screenshot . '.' . $n . '.png';

      $this->captureScreenshotOfBlock($block, $screenshot_n);

      $diff = $n === 1
        ? 1
        : $this->compareScreenshot($screenshot_n, $screenshot);
      if ($diff > $diff_threshold) {
        if (!copy($screenshot_n, $screenshot)) {
          throw new RuntimeException(sprintf('Failed to copy %s to %s', $screenshot_n, $screenshot));
        }
        $consecutive_match_count = 0;
        continue;
      }

      $consecutive_match_count++;
      if ($consecutive_match_count === $consecutive_match_threshold) {
        if (!copy($screenshot, $reference)) {
          throw new RuntimeException(sprintf('Failed to copy %s to %s', $screenshot, $reference));
        }
        return $n;
      }
    }

    throw new RuntimeException('Failed to create a consistent screenshot');
  }

  public function captureAndCompareScreenshotOfBlock(string $block, string $screenshot, string $reference, float $threshold = 0): array {
    $max_tries = 3;

    $n = 0;
    $diffs = [];
    while ($n < $max_tries) {
      $n++;
      $screenshot_n = $screenshot . '.' . $n . '.png';

      $this->captureScreenshotOfBlock($block, $screenshot_n);

      $diff = $diffs[] = $this->compareScreenshot($screenshot_n, $reference, $threshold);
      if ($diff <= $threshold) {
        return $diffs;
      }
    }

    return $diffs;
  }

  public function compareScreenshot(string $screenshot, string $reference, $threshold = 0): float {
    $screenshot_img = new imagick($screenshot);
    $reference_img = new imagick($reference);

    [$diff_img, $diff] = $screenshot_img->compareImages($reference_img, Imagick::METRIC_MEANSQUAREERROR);

    if ($diff > $threshold) {
      assert($diff_img instanceof Imagick);
      $diff_img->setImageFormat('png');
      $diff_img->writeImage($screenshot . '.diff.png');

      return $diff;
    }

    if (file_exists($screenshot . '.diff.png')) {
      unlink($screenshot . '.diff.png');
    }

    return $diff;
  }

  public function captureScreenshotOfBlock(string $block, string $filename) {
    $driver = $this->getChromeDriver();

    [$left, $top, $width, $height] = $driver->evaluateScript(<<<ENDJS
      var rect = document.querySelector('.$block').getBoundingClientRect();
      [rect.left, rect.top, rect.width, rect.height];
      ENDJS
    );

    $driver->captureScreenshot($filename, [
      'captureBeyondViewport' => TRUE,
      'clip' => [
        'x' => floor($left),
        'y' => floor($top),
        'width' => ceil($width),
        'height' => ceil($height),
        'scale' => 1,
      ]
    ]);
  }

  public function emulateDevice(string $name): void {
    $device = $this->devices[$name];

    $this->currentDevice = $name;

    $page = $this->getChromePage();
    $page->send('Emulation.setDeviceMetricsOverride', [
      'width'             => $device['width'],
      'height'            => $device['height'],
      'deviceScaleFactor' => 0,
      'mobile'            => $device['mobile'],
      'fitWindow'         => false,
    ]);
    $page->send('Emulation.setVisibleSize', [
      'width'  => $device['width'],
      'height' => $device['height'],
    ]);

    usleep(100);
  }

  public function getScreenshotName(): string {
    return sprintf(
      '%s/%s/%s.png',
      $this->currentFeature,
      $this->currentScenario,
      $this->devices[$this->currentDevice]['width'],
    );
  }

  public function getScreenshotPath(string $name): string {
    return $this->screenshotDir . '/' . $name;
  }

  public function getReferencePath(string $name): string {
    return $this->referenceDir . '/' . $name;
  }

  private function ensureDirectory(string $path): void {
    if (!is_dir($path) && !mkdir($path, 0755, TRUE)) {
      throw new RuntimeException(sprintf('Failed to create directory: %s', $path));
    }
  }

  private function getChromeDriver(): ChromeDriver {
    $driver = $this->getSession()->getDriver();
    assert($driver instanceof ChromeDriver);
    return $driver;
  }

  private function getChromePage(): ChromePage {
    $driver = $this->getChromeDriver();

    $property = new ReflectionProperty(ChromeDriver::class, 'page');
    $property->setAccessible(TRUE);

    $page = $property->getValue($driver);
    assert($page instanceof ChromePage);

    return $page;
  }

}
