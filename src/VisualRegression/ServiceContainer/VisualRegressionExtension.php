<?php

namespace Fluo\Behat\VisualRegression\ServiceContainer;

use Behat\Behat\Context\ServiceContainer\ContextExtension;
use Behat\Testwork\ServiceContainer\Extension;
use Behat\Testwork\ServiceContainer\ExtensionManager;
use Fluo\Behat\VisualRegression\Context\ScreenshotContextInitializer;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class VisualRegressionExtension implements Extension {

  public function getConfigKey(): string {
    return 'fluo_visual_regression';
  }

  public function initialize(ExtensionManager $extensionManager): void {

  }

  public function configure(ArrayNodeDefinition $builder) {
    $builder
      ->children()
      ->scalarNode('screenshot_dir')
      ->isRequired()
      ->info('Sets the folder used to store screenshot');

    $builder
      ->children()
      ->scalarNode('reference_dir')
      ->isRequired()
      ->info('Sets the folder containing reference screenshots');

    $devicesBuilder = $builder
      ->children()
      ->arrayNode('devices')
      ->isRequired()
      ->requiresAtLeastOneElement()
      ->useAttributeAsKey('name')
      ->arrayPrototype();

    $devicesBuilder
      ->children()
      ->integerNode('width')->isRequired()->end()
      ->integerNode('height')->isRequired()->end()
      ->booleanNode('mobile')->defaultFalse()->end();
  }

  public function load(ContainerBuilder $container, array $config): void {
    $container->setParameter("fluo_visual_regression.parameters", $config);

    $definition = new Definition(ScreenshotContextInitializer::class, [
      '%fluo_visual_regression.parameters%',
    ]);
    $definition->addTag(ContextExtension::INITIALIZER_TAG, ['priority' => 0]);
    $container->setDefinition('fluo_visual_regression.screenshot_context_initializer', $definition);
  }

  public function process(ContainerBuilder $container): void {

  }

}
