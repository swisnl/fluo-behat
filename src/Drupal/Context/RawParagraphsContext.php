<?php

namespace Fluo\Behat\Drupal\Context;

use Behat\Gherkin\Node\TableNode;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\DrupalExtension\Context\RawDrupalContext;
use Drupal\DrupalExtension\Hook\Scope\EntityScope;
use RuntimeException;
use stdClass;

abstract class RawParagraphsContext extends RawDrupalContext {

  protected array $paragraphs = [];

  protected ?stdClass $currentNode;

  /**
   * Remove any created paragraphs.
   *
   * @AfterScenario
   *
   * @see \Drupal\DrupalExtension\Context\RawDrupalContext::cleanNodes()
   */
  public function cleanParagraphs(): void {
    foreach ($this->paragraphs as $paragraph) {
      $this->getDriver()->entityDelete('paragraph', $paragraph);
    }
    $this->paragraphs = [];
  }

  /**
   * @see \Drupal\DrupalExtension\Context\RawDrupalContext::nodeCreate()
   */
  public function paragraphCreate(stdClass $paragraph): stdClass {
    $ref = $paragraph->{'$ref'} ?? '$' . count($this->paragraphs);

    if (isset($this->paragraphs[$ref])) {
      throw new RuntimeException(sprintf('Reference "%s" is already being used for another paragraph', $ref));
    }

    $this->parseEntityFields('paragraph', $paragraph);
    $saved = $this->getDriver()->createEntity('paragraph', $paragraph);
    $this->paragraphs[$ref] = $saved;
    return $saved;
  }

  public function paragraphsCreate(array $paragraphs): array {
    return array_map(
      fn(stdClass $paragraph) => $this->paragraphCreate($paragraph),
      $paragraphs
    );
  }

  public function getParagraphByRef(string $ref): stdClass {
    if (!isset($this->paragraphs[$ref])) {
      throw new RuntimeException(sprintf('Paragraph with reference "%s" does not exist', $ref));
    }
    return $this->paragraphs[$ref];
  }

  /**
   * Keeps track of the last created node.
   *
   * @afterNodeCreate
   */
  public function afterNodeCreate(EntityScope $scope): void {
    $this->currentNode = $scope->getEntity();
  }

  public function getCurrentNode(): stdClass {
    if (!$this->currentNode) {
      throw new RuntimeException('No current node.');
    }

    return $this->currentNode;
  }

  public function appendParagraphToCurrentNode(string $field_name, stdClass $paragraph): void {
    $this->appendParagraphs(
      'node',
      $this->getCurrentNode()->nid,
      $field_name,
      [$paragraph]
    );
  }

  public function appendParagraphs(string $entity_type, string $entity_id, string $field_name, array $paragraphs): void {
    $entity = $this->loadEntity($entity_type, $entity_id);

    $field = $entity->get($field_name);
    foreach ($this->loadEntities('paragraph', $paragraphs) as $paragraph) {
      $field->appendItem($paragraph);
    }

    $entity->save();

    $this->clearStaticCaches();
  }

  private function loadEntity(string $entity_type, string $entity_id): ContentEntityInterface {
    $entity = \Drupal::entityTypeManager()
      ->getStorage($entity_type)
      ->load($entity_id);
    assert($entity instanceof ContentEntityInterface);
    return $entity;
  }

  private function loadEntities(string $entity_type, array $entity_objects): array {
    return \Drupal::entityTypeManager()
      ->getStorage($entity_type)
      ->loadMultiple($this->getEntityIds($entity_objects));
  }

  private function getEntityIds(array $entity_objects): array {
    return array_map(
      fn (stdClass $item) => $item->id,
      $entity_objects
    );
  }

  public function castParagraphTable(TableNode $fields, array $defaults): stdClass {
    return (object) array_merge($defaults, $fields->getRowsHash());
  }

  public function castParagraphsTable(TableNode $table, array $defaults = []): array {
    $paragraphs = [];
    foreach ($table->getColumnsHash() as $fields) {
      $paragraphs[] = (object) array_merge($defaults, $fields);
    }
    return $paragraphs;
  }

}
