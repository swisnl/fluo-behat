<?php

namespace Fluo\Behat\Drupal\Context;

use Behat\Gherkin\Node\TableNode;

/**
 * Provides step-definitions for interacting with Drupal paragraphs.
 */
class ParagraphsContext extends RawParagraphsContext {

  /**
   * Adds a paragraph of given type to given field of the last viewed content.
   *
   * The paragraph should be provided in the form:
   * | title     | My paragraph   |
   * | field_one | My field value |
   * | status    | 1              |
   * | ...       | ...            |
   *
   * @link /vendor/drupal/drupal-extension/features/field_handlers.feature
   *
   * @Given /^the "(?P<field>[^"]*)"( field)? contains a "(?P<type>[^"]*)" paragraph:$/
   */
  public function fieldOfCurrentNodeContainsParagraph(string $field, string $type, TableNode $fields = null): void {
    $this->appendParagraphToCurrentNode(
      $field,
      $this->paragraphCreate($this->castParagraphTable($fields, [
        'type' => $type,
      ]))
    );

    $this->getSession()->reload();
  }

  /**
   * Adds paragraphs to given field of paragraph with given ref.
   *
   * The paragraph should be provided in the form:
   * | type     | title       | field_one |
   * | bundle_a | Paragraph 1 | Foo       |
   * | bundle_b | Paragraph 2 | Bar       |
   *
   * @throws \Exception
   *
   * @Given /^the "(?P<field>[^"]*)" field of paragraph "(?P<ref>[^"]*)" contains:$/
   */
  public function paragraphFieldContains(string $field, string $ref, TableNode $paragraphsTable): void {
    $this->appendParagraphs(
      'paragraph',
      $this->getParagraphByRef($ref)->id,
      $field,
      $this->paragraphsCreate($this->castParagraphsTable($paragraphsTable))
    );

    $this->getSession()->reload();
  }

}
