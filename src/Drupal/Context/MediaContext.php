<?php

namespace Fluo\Behat\Drupal\Context;

use Behat\Gherkin\Node\TableNode;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\DrupalExtension\Context\RawDrupalContext;
use Drupal\file\FileInterface;
use Drupal\file\FileRepositoryInterface;
use RuntimeException;

/**
 * Provides step-definitions for interacting with Drupal media entities.
 */
class MediaContext extends RawDrupalContext {

  private EntityTypeManagerInterface $entityTypeManager;

  private FileRepositoryInterface $fileRepository;

  protected array $files = [];

  protected array $media = [];

  public function __construct() {
    $this->entityTypeManager = \Drupal::entityTypeManager();
    $this->fileRepository = \Drupal::service('file.repository');
  }

  /**
   * Remove any created files.
   *
   * @AfterScenario
   */
  public function cleanFiles(): void {
    $storage = $this->entityTypeManager->getStorage('file');
    foreach ($this->files as $file) {
      // Check if the entity still exists before deleting it.
      if ($storage->load($file->id()) !== NULL) {
        $file->delete();
      }
    }

    $this->files = [];
  }

  public function fileCreate(string $file_name): FileInterface {
    $file_path = $this->getMinkParameter('files_path') . '/' . $file_name;
    if (!file_exists($file_path)) {
      throw new RuntimeException(sprintf('Missing file "%s', $file_path));
    }

    $file = $this->fileRepository->writeData(file_get_contents($file_path), 'public://' . $file_name);
    $file->setPermanent();
    $file->save();

    $this->files[] = $file;

    return $file;
  }

  /**
   * Remove any created media entities.
   *
   * @AfterScenario
   *
   * @see \Drupal\DrupalExtension\Context\RawDrupalContext::cleanNodes()
   */
  public function cleanMedia(): void {
    foreach ($this->media as $media) {
      $media->delete();
    }
    $this->media = [];
  }

  /**
   * Creates media images with the specified field values.
   *
   * Usage example:
   *
   * Given the following images:
   *   | name   | file   | alt   | title   |
   *   | name 1 | file 1 | alt 1 | title 1 |
   *   | ...    | ...    | ...   | ...     |
   *
   * Properties "alt" and "title" are optional, "name" will be used if they
   * are not provided.
   *
   * @Given the following image(s):
   */
  public function createMediaImages(TableNode $table): void {
    foreach ($table->getColumnsHash() as $properties) {
      $file = $this->fileCreate($properties['file']);

      $media = $this->entityTypeManager->getStorage('media')->create([
        'bundle' => 'image',
        'name' => $properties['name'],
        'status' => 1,
        'image__source' => [
          'target_id' => $file->id(),
          'alt' => $properties['alt'] ?? $properties['name'],
          'title' => $properties['title'] ?? $properties['name'],
        ],
      ]);
      $media->save();

      $this->media[] = $media;
    }
  }

}
